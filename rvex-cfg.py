#!/usr/bin/env python3

import sys
import json

if __name__ == '__main__':
    
    # Check command line/print usage.
    if len(sys.argv) < 2:
        print('Usage: %s <cfg.json>' % sys.argv[0], file=sys.stderr)
        print('', file=sys.stderr)
        print('Configures the source tree in the current directory according to the given', file=sys.stderr)
        print('configuration file. Returns 0 if successful, 3 if the tool should not be', file=sys.stderr)
        print('installed, or something else (usually 1) if an error occurred.', file=sys.stderr)
        print('', file=sys.stderr)
        print('You can also run `%s -` to have the script generate default versions of the', file=sys.stderr)
        print('source files. These are the ones that should be checked into the git repos.', file=sys.stderr)
        sys.exit(2)
    
    # Load the configuration file.
    if sys.argv[1].strip() == '-':
        cfg_json = {}
    else:
        with open(sys.argv[1], 'r') as f:
            cfg_json = json.load(f)

class Config(object):
    
    def __init__(self, d, prefix=''):
        self._d = d
        self._p = prefix
        self._s = set()
        for key in d:
            if not key.startswith(prefix):
                continue
            key = key[len(prefix):].split('.')
            if len(key) == 1:
                continue
            self._s.add(key[0])
    
    def __getattr__(self, name):
        val = self._d.get(self._p + name, None)
        if val is not None:
            return val
        
        if name in self._s:
            return Config(self._d, self._p + name + '.')
        
        raise AttributeError(name)
    
    def __str__(self):
        data = ['    %s: %s\n' % (key[len(self._p):], value) for key, value in sorted(self._d.items()) if key.startswith(self._p)]
        return 'Config(\n' + ''.join(data) + ')'

def parse_cfg(cfg_json, defaults):
    """Parses the config.json contents into a Config object that is guaranteed
    to contain the keys in `defaults`. An error is printed if the given cfg_json
    dict contains non-default keys which are not present in the `defaults`
    dict, which implies that the configuration file has a higher version than
    this file and is doing something that isn't backwards compatible."""
    
    # Check that we don't have any "required" keys that we don't know about.
    for key, value in cfg_json.items():
        if value['req'] and key not in defaults:
            print('Error: found non-default key "%s", which this version of rvex-cfg.py doesn\'t know about!' % key, file=sys.stderr)
            sys.exit(1)
    
    # Build our cfg dictionary.
    cfg = {}
    for key, default in defaults.items():
        value = cfg_json.get(key, {'val': default})
        if value is None:
            print('Error: no configuration value specified for key "%s"!' % key, file=sys.stderr)
            sys.exit(1)
        cfg[key] = value['val']
    
    return Config(cfg)

def template(infname, outfname, *args, **kwargs):
    """Template engine simply using Python's str.format()."""
    with open(infname, 'r') as inf:
        with open(outfname, 'w') as outf:
            outf.write(inf.read().format(*args, **kwargs))

def template2(infname, outfname, *args, **kwargs):
    """Template engine using Python's str.format(), but with different
    characters that are more suitable for C:
     - open:   <|
     - close:  |>
     - escape: <-| to <|
               |-> to |>
    """
    with open(infname, 'r') as inf:
        with open(outfname, 'w') as outf:
            data = inf.read()
            data = data.replace('{', '{{').replace('}', '}}')
            data = data.replace('<|', '{').replace('|>', '}')
            data = data.format(*args, **kwargs)
            data = data.replace('<-|', '<|').replace('|->', '|>')
            outf.write(data)

def dont_build(s=''):
    if s:
        s = ': ' + s
    print('This tool does not need to be built%s' % s, file=sys.stderr)
    sys.exit(3)

def misconfigured(s):
    if s:
        s = ': ' + s
    print('Configuration error%s' % s, file=sys.stderr)
    sys.exit(1)

def done(s):
    if s:
        s = ': ' + s
    print('Configuration complete%s' % s, file=sys.stderr)
    sys.exit(0)

#===============================================================================
# Tool-specific from here onwards
#===============================================================================

cfg = parse_cfg(cfg_json, {
    
    # Vexparse configuration.
    'toolchain.vexparse_opt': 0,
    'toolchain.vexparse_unstable': False,
    
    # Number of lanes.
    'resources.num_lanes': 8,
    
    # Lane resources (list of boolean).
    'resources.int_mul_lanes': [True]*8,
    'resources.float_add_lanes': [False]*8,
    'resources.float_mul_lanes': [False]*8,
    'resources.float_cmp_lanes': [False]*8,
    'resources.float_i2f_lanes': [False]*8,
    'resources.float_f2i_lanes': [False]*8,
    
    # Branch unit configuration.
    'resources.bundle_align': 2,
    
    # Memory lane configuration.
    'resources.num_groups': 4,
    'resources.mem_lane': 0,
    'resources.mem_one_per_cyc': True,
    
    # Borrow configuration.
    'resources.limmh_prev': False,
    'resources.limmh_neighbor': True,
    
    # Latency configuration.
    'special.disable_forward': False,
    'special.extend_pipeline': False
    
})

# Don't build if the user doesn't want us.
if cfg.toolchain.vexparse_opt == -1:
    template('Makefile.in.tpl', 'Makefile.in', target='no-vexparse')
    done('vexparse is disabled (toolchain.vexparse_opt)')
else:
    template('Makefile.in.tpl', 'Makefile.in', target='vexparse')

# Refuse to build if there are non-default latencies and the unstable flag is not set.
if (cfg.special.disable_forward or cfg.special.extend_pipeline) and not cfg.toolchain.vexparse_unstable:
    misconfigured('vexparse currently does not properly support non-default '
        'latencies. Set "toolchain.vexparse_unstable" to override.')

# Put the default configuration dictionary together.
config_py = ['from instructions import FU\n\ndefault_config = {']

# Long immediate/borrow configuration.
config_py.append('    \'borrow\': [')
for insn_lane in range(cfg.resources.num_lanes):
    limmh_lanes = []
    if cfg.resources.limmh_prev:
        if insn_lane >= 2:
            limmh_lanes.append(insn_lane - 2)
    if cfg.resources.limmh_neighbor:
        limmh_lanes.append(insn_lane ^ 1)
    config_py.append('        %s,' % limmh_lanes)
config_py.append('    ],')

# Lane configuration/layout.
config_py.append('    \'layout\': [')
for lane in range(cfg.resources.num_lanes):
    fus = []
    
    # ALU: always.
    fus.append('FU.ALU')
    
    # MUL: depends on multiplier config.
    if cfg.resources.int_mul_lanes[lane]:
        fus.append('FU.MUL')
    
    # MEM: depends on memory config.
    lane_in_group = lane % (cfg.resources.num_lanes // cfg.resources.num_groups)
    if lane_in_group == cfg.resources.mem_lane:
        fus.append('FU.MEM')
    
    # BR: depends on bundle alignment.
    if lane & (cfg.resources.bundle_align-1) == cfg.resources.bundle_align-1:
        fus.append('FU.BR')
    
    # Float units: depends on float unit configs.
    if cfg.resources.float_add_lanes[lane]:
        fus.append('FU.FADD')
    if cfg.resources.float_mul_lanes[lane]:
        fus.append('FU.FMUL')
    if cfg.resources.float_cmp_lanes[lane]:
        fus.append('FU.FCMP')
    if cfg.resources.float_i2f_lanes[lane]:
        fus.append('FU.I2F')
    if cfg.resources.float_f2i_lanes[lane]:
        fus.append('FU.F2I')
    
    config_py.append('        {%s},' % ', '.join(fus))
config_py.append('    ],')

# Functional unit counts.
config_py.append('    \'fus\': {')
config_py.append('        FU.ALU:  %d,' % cfg.resources.num_lanes)
config_py.append('        FU.MUL:  %d,' % sum(cfg.resources.int_mul_lanes))
config_py.append('        FU.MEM:  %d,' % (1 if cfg.resources.mem_one_per_cyc else cfg.resources.num_groups))
config_py.append('        FU.BR:   1,')
config_py.append('        FU.FADD: %d,' % sum(cfg.resources.float_add_lanes))
config_py.append('        FU.FMUL: %d,' % sum(cfg.resources.float_mul_lanes))
config_py.append('        FU.FCMP: %d,' % sum(cfg.resources.float_cmp_lanes))
config_py.append('        FU.I2F:  %d,' % sum(cfg.resources.float_i2f_lanes))
config_py.append('        FU.F2I:  %d,' % sum(cfg.resources.float_f2i_lanes))
config_py.append('    },')

# Functional unit latencies.
# TODO: these need to be checked! Also in open64/rvex-cfg.py.
short_lat = 4 if cfg.special.disable_forward else 1
long_lat  = 4 if cfg.special.disable_forward else 2
if cfg.special.extend_pipeline:
    short_lat += 2
    long_lat  += 2
config_py.append('    \'latency\': {')
config_py.append('        FU.ALU:  %d,' % short_lat)
config_py.append('        FU.MUL:  %d,' % long_lat)
config_py.append('        FU.MEM:  %d,' % long_lat)
config_py.append('        FU.BR:   1,')
config_py.append('        FU.FADD: %d,' % long_lat)
config_py.append('        FU.FMUL: %d,' % long_lat)
config_py.append('        FU.FCMP: %d,' % long_lat)
config_py.append('        FU.I2F:  %d,' % long_lat)
config_py.append('        FU.F2I:  %d,' % short_lat)
config_py.append('    },')

# Optimization level.
config_py.append('    \'opt\': %d,' % max(0, cfg.toolchain.vexparse_opt))

# Number of lanes.
config_py.append('    \'max-issue\': %d' % cfg.resources.num_lanes)

# Write the configuration file.
config_py.append('}')
with open('config.py', 'w') as f:
    f.write('\n'.join(config_py))

done('vexparse is enabled')
