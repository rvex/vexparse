vexparse: r-VEX generic binary optimizer
========================================

This repository contains vexparse, a Python program that operates on r-VEX
assembly files to reschedule them according to generic binary rules. It is
somewhat hacked together and unstable, though.


Configuration and build process
-------------------------------

Since vexparse is a Python program, there is no real build process; you can just
call `main.py` from this directory and it'll work just fine. However, the
repository contains the requisite `rvex-cfg.py` and `configure` commands such
that the tool builder can install it without magic.
