import re
import parse
import vex
import enum

class FU(enum.IntEnum):
    ALU   = 1
    MUL   = 2
    MEM   = 4
    BR    = 8
    FADD  = 16
    FMUL  = 32
    FCMP  = 64
    I2F   = 128
    F2I   = 256

# This should be updated by main()!
LATENCIES = {fu: 1 for fu in FU}

def parse_instruction(line, comment="", line_no=0):
    cluster, line2 = parse.get_cluster(line)
    mnemonic, line2 = parse.get_mnemonic(line2)
    if not mnemonic:
        print("Error parsing line {}\n".format(line_no))
        print(line)
        exit(1)
    cls = {
        'add':          AluInstruction,
        'addcg':        AluInstruction,
        'addf':         FAddInstruction,
        'and':          AluInstruction,
        'andc':         AluInstruction,
        'andl':         AluInstruction,
        'br':           BranchInstruction,
        'brf':          BranchInstruction,
        'call':         CallInstruction,
        'clz':          AluInstruction,
        'cmpeq':        AluInstruction,
        'cmpeqf':       FCmpInstruction,
        'cmpge':        AluInstruction,
        'cmpgef':       FCmpInstruction,
        'cmpgeu':       AluInstruction,
        'cmpgt':        AluInstruction,
        'cmpgtf':       FCmpInstruction,
        'cmpgtu':       AluInstruction,
        'cmple':        AluInstruction,
        'cmplef':       FCmpInstruction,
        'cmpleu':       AluInstruction,
        'cmplt':        AluInstruction,
        'cmpltf':       FCmpInstruction,
        'cmpltu':       AluInstruction,
        'cmpne':        AluInstruction,
        'convbi':       AluInstruction,
        'convfi':       F2IInstruction,
        'convib':       AluInstruction,
        'convif':       I2FInstruction,
        'divs':         AluInstruction,
        'goto':         GotoInstruction,
        'ldb':          LoadInstruction,
        'ldbr':         LoadInstruction,
        'ldbu':         LoadInstruction,
        'ldh':          LoadInstruction,
        'ldhu':         LoadInstruction,
        'ldw':          LoadInstruction,
        'max':          AluInstruction,
        'maxu':         AluInstruction,
        'mfb':          AluInstruction,
        'min':          AluInstruction,
        'minu':         AluInstruction,
        'mov':          AluInstruction,
        'movfl':        AluInstruction,
        'movtl':        AluInstruction,
        'mpyf':         FMulInstruction,
        'mpyh':         MulInstruction,
        'mpyhh':        MulInstruction,
        'mpyhhs':       MulInstruction,
        'mpyhhu':       MulInstruction,
        'mpyhs':        MulInstruction,
        'mpyhu':        MulInstruction,
        'mpyl':         MulInstruction,
        'mpylh':        MulInstruction,
        'mpylhu':       MulInstruction,
        'mpylhus':      MulInstruction,
        'mpyll':        MulInstruction,
        'mpyllu':       MulInstruction,
        'mpylu':        MulInstruction,
        'mtb':          AluInstruction,
        'nandl':        AluInstruction,
        'nop':          NopInstruction,
        'norl':         AluInstruction,
        'or':           AluInstruction,
        'orc':          AluInstruction,
        'orl':          AluInstruction,
        'return':       ReturnInstruction,
        'rfi':          ReturnInstruction,
        'sbit':         AluInstruction,
        'sbitf':        AluInstruction,
        'sh1add':       AluInstruction,
        'sh2add':       AluInstruction,
        'sh3add':       AluInstruction,
        'sh4add':       AluInstruction,
        'shl':          AluInstruction,
        'shr':          AluInstruction,
        'shru':         AluInstruction,
        'slct':         AluInstruction,
        'slctf':        AluInstruction,
        'stb':          StoreInstruction,
        'stbr':         StoreInstruction,
        'sth':          StoreInstruction,
        'stop':         StopInstruction,
        'stw':          StoreInstruction,
        'sub':          AluInstruction,
        'subf':         FAddInstruction,
        'sxtb':         AluInstruction,
        'sxth':         AluInstruction,
        'tbit':         AluInstruction,
        'tbitf':        AluInstruction,
        'trap':         TrapInstruction,
        'xor':          AluInstruction,
        'zxtb':         AluInstruction,
        'zxth':         AluInstruction
    }.get(mnemonic.lower().strip().split('.', 1)[0], None)
    
    if cls is None:
        print("Error parsing line {}\n".format(line_no))
        print(line)
        exit(1)
    
    return cls(line, comment, line_no)

class Instruction:
    """Object for holding and parsing VEX instructions.
    """

    def __eq__(self, other):
        if not isinstance(other, Instruction):
            return False
        return str(self) == str(other)

    def get_fu(self):
        return FU.ALU

    def change_source_reg(self, orig, new):
        self.srcs = [new if arg == orig else arg for arg in self.srcs]

    def change_dest_reg(self, orig, new):
        self.dests = [new if arg == orig else arg for arg in self.dests]

    def is_branch(self):
        return False

    def is_call(self):
        return False

    def is_return(self):
        return False

    def get_branch_destination(self):
        """Return the branch destination of this instruction"""
        return ["next"]

    def get_written_registers(self):
        return {x for x in self.dests
                if isinstance(x, vex.Register)}

    def get_read_registers(self):
        return {x for x in self.srcs if isinstance(x, vex.Register)}

    def has_dest_args(self):
        return True

    def parse_args(self, args):
        dest = self.has_dest_args()
        for arg in [x.strip() for x in args]:
            if arg == "," or arg == '[' or arg == ']' or arg == '':
                continue
            if arg == '=':
                dest = False
                continue
            if parse.is_register(arg):
                arg = vex.parse_register(arg)
            if dest:
                self.dests.append(arg)
            else:
                self.srcs.append(arg)
    
    def cost(self):
        return LATENCIES[self.get_fu()]

    def arg_is_long_imm(self, arg):
        if isinstance(arg, vex.Register):
            return False
        try:
            a = eval(arg, {"__builtins__":None}, None)
            if a > 255 or a < -256:
                return True
            else:
                return False
        except:
            return True

    def has_long_imm(self):
        return self.has_long

    def __init__(self, line, comment, line_no):
        self.dests = []
        self.srcs = []
        tokens = re.split(r'(?:,\s*)|\s+', line.strip())
        if not parse.get_cluster(line):
            print(line, file=sys.stderr)
        self.cluster, line = parse.get_cluster(line)
        self.mnemonic, line = parse.get_mnemonic(line)
        self.parse_args(re.split(r"([,=\[\]])", line))
        self.has_long = any(self.arg_is_long_imm(arg) for arg in self.srcs)
        self.comment = comment
        self.line_no = line_no
        self.pseudo_op = None

    def __str__(self):
        string = ''
        if self.pseudo_op:
            string = self.pseudo_op + '\n'
        string += "c{0} {1} ".format(self.cluster, self.mnemonic)
        string += ', '.join(str(x) for x in self.dests)
        if len(self.dests) > 0:
            string += ' = '
        if len(self.srcs) > 0:
            string += ', '.join(str(x) for x in self.srcs)
        if self.comment:
            string += ' #' + self.comment
        return string

    def __repr__(self):
        return "{0}('{1}')".format(self.__class__.__name__, str(self))

class AluInstruction(Instruction):
    def get_fu(self):
        return FU.ALU

class MulInstruction(Instruction):
    def get_fu(self):
        return FU.MUL

class FAddInstruction(Instruction):
    def get_fu(self):
        return FU.FADD

class FMulInstruction(Instruction):
    def get_fu(self):
        return FU.FMUL

class FCmpInstruction(Instruction):
    def get_fu(self):
        return FU.FCMP

class I2FInstruction(Instruction):
    def get_fu(self):
        return FU.I2F

class F2IInstruction(Instruction):
    def get_fu(self):
        return FU.F2I

class ControlInstruction(Instruction):
    #
    def has_long_imm(self):
        return False

    def is_branch(self):
        return True

    def get_fu(self):
        return FU.BR
    
    def cost(self):
        return 1

class CallInstruction(ControlInstruction):
    #
    def is_call(self):
        return True

class BranchInstruction(ControlInstruction):
    #
    def get_branch_destination(self):
        """Return the branch destination of this instruction"""
        return ["next", self.srcs[-1]]

    def parse_args(self, args):
        for arg in [x.strip() for x in args]:
            if (arg == "," or arg == '[' or arg == ']' or
                arg == '' or arg == '='):
                continue
            if parse.is_register(arg):
                arg = vex.parse_register(arg)
            self.srcs.append(arg)

    def __str__(self):
        string = "c{0} {1} ".format(self.cluster, self.mnemonic)
        string += ", ".join(str(x) for x in self.srcs)
        return string

class ReturnInstruction(ControlInstruction):
    #
    def get_written_registers(self):
        if len(self.dests) == 0:
            return {vex.GeneralRegister(0, 1)}
        else:
            return super().get_written_registers()

    def get_read_registers(self):
        regs = super().get_read_registers()
        if len(self.srcs) == 1:
            regs.add(vex.GeneralRegister(0,1))
        return regs

    def is_return(self):
        return True

    def get_branch_destination(self):
        """Return the branch destination of this instruction"""
        return ["return"]

    def parse_args(self, args):
        super().parse_args(args)
        if len(args) == 1:
            temp = self.srcs
            self.srcs = self.dests
            self.dests = temp

class GotoInstruction(BranchInstruction):
    #
    def get_branch_destination(self):
        """Return the branch destination of this instruction"""
        if self.srcs[-1] == '1-1':
            return ['next']
        return [self.srcs[-1]]

class StoreInstruction(Instruction):
    #
    def get_fu(self):
        return FU.MEM

    def parse_args(self, args):
        for arg in [x.strip() for x in args]:
            if (arg == "," or arg == '[' or arg == ']' or
                arg == '' or arg == '='):
                continue
            if parse.is_register(arg):
                arg = vex.parse_register(arg)
            self.srcs.append(arg)

    def __str__(self):
        string = "c{0} {1} ".format(self.cluster, self.mnemonic)
        string += "{0}[{1}] = {2}".format(self.srcs[0], self.srcs[1],
                                          self.srcs[2])
        return string

class LoadInstruction(Instruction):
    #
    def get_fu(self):
        return FU.MEM

    def cost(self):
        return LATENCIES[FU.MEM]

    def __str__(self):
        string = "c{0} {1} ".format(self.cluster, self.mnemonic)
        string += "{0} = {1}[{2}]".format(self.dests[0], self.srcs[0],
                                          self.srcs[1])
        return string

class StopInstruction(Instruction):
    #
    def get_fu(self):
        return FU.BR

    def __str__(self):
        return 'c{0} {1}'.format(self.cluster, self.mnemonic)
        string = 'c{0} {1}'.format(self.cluster, self.mnemonic)
        if self.comment:
            string += ' #' + self.comment
        return string

class NopInstruction(Instruction):
    #
    def get_fu(self):
        return FU.ALU

    def __str__(self):
        string = 'c{0} {1}'.format(self.cluster, self.mnemonic)
        if self.comment:
            string += ' #' + self.comment
        return string

class TrapInstruction(ControlInstruction):
    #
    def get_branch_destination(self):
        """Return the branch destination of this instruction"""
        return ['#trap_handler']

    def has_dest_args(self):
        return False

class EndBBInstruction(Instruction):
    #
    def __init__(self):
        self.line_no = -1
        pass

    def __str__(self):
        return ''

