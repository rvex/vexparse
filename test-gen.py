import random
import subprocess
import threading
import re
import sys

max_depth = 3

class For:

    def get_idx(self):
        idxs = set()
        if not self.only_body:
            idxs.add(self.depth)
        return idxs | self.body.get_idx()


    def get_vars(self):
        return self.body.get_vars()

    def __init__(self, vars, depth):
        self.depth = depth
        self.body = Body(vars, depth+1)
        self.only_body = False

    def __str__(self):
        if self.only_body:
            return str(self.body)
        string = """for (i{0} = 0; i{0} < 10; i{0}++){{\n{1}\n}}"""
        return string.format(self.depth, self.body)

class Body:

    def get_idx(self):
        idxs = set()
        for stmnt in self.stmnts:
            idxs |= stmnt.get_idx()
        return idxs

    def get_vars(self):
        vars = set()
        for stmnt in self.stmnts:
            vars |= stmnt.get_vars()
        return vars

    def __init__(self, max_var, depth):
        self.stmnts = []
        for i in range(random.randint(1, 5)):
            self.stmnts.append(make_stmnt(max_var, depth))

    def __str__(self):
        return "\n".join(str(x) for x in self.stmnts)

class If:

    def get_idx(self):
        return self.body.get_idx()

    def get_vars(self):
        vars = {self.a, self.b}
        return vars | self.body.get_vars()

    def __init__(self, max_var, depth):
        self.a = random.randint(0, max_var)
        self.cmp = random.choice(['==', '>=', '<=', '<', '>'])
        self.b = random.randint(0, max_var)
        self.body = Body(max_var, depth+1)
        self.only_body = False

    def __str__(self):
        if self.only_body:
            return str(self.body)
        string = """if (a{0} {1} a{2}){{\n{3}\n}}"""
        return string.format(self.a, self.cmp, self.b, self.body)

class Assign:

    def get_idx(self):
        return set()

    def get_vars(self):
        return {self.a, self.b, self.c}

    def __init__(self, max_var, depth):
        self.a = random.randint(0, max_var)
        self.b = random.randint(0, max_var)
        self.op = random.choice(['+', '-', '*', '/'])
        self.c = random.randint(0, max_var)
        self.body = []

    def __str__(self):
        string = 'a{0} = a{1} {2} a{3};'
        return string.format(self.a, self.b, self.op, self.c)

def make_stmnt(max_var, depth):
    if depth >= max_depth:
        return Assign(max_var, depth)
    else:
        return random.choice([Assign, If, For])(max_var, depth)

class Number:

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return str(self.value)

class Main:

    def declare_vars(self):
        result = []
        for x in self.body.get_vars():
            result.append("int a{} = {!s};".format(x, self.values[x]))
        return '\n'.join(result)

    def declare_idx(self):
        result = []
        for x in self.body.get_idx():
            result.append("int i{0} = 0;".format(x))
        return '\n'.join(result)

    def make_return(self):
        return " ^ ".join("a{}".format(x) for x in self.body.get_vars())

    def __init__(self):
        self.state = random.getstate()
        self.max_var = random.randint(3,15)
        self.values = [Number(random.randint(-128, 127)) for x in
                range(self.max_var + 1)]
        self.body = Body(self.max_var, 1)

    def __str__(self):
        string = """{0}\nint main(void)\n{{\n{1}\n{2}\nreturn {3};\n}}"""
        init = self.declare_vars()
        init2 = self.declare_idx()
        return string.format(init, init2, self.body, self.make_return())

class Func:

    def __init__(self):
        pass

class Program:

    def __init__(self):
        pass

class IfSimplifier:

    def next(self):
        if self.simp:
            try:
                self.simp.next()
                return
            except StopIteration:
                self.simp = None
        if self.stop:
            raise StopIteration
        self.gen.only_body = True
        self.stop = True

    def undo(self):
        if self.simp:
            self.simp.undo()
        else:
            self.gen.only_body = False

    def __init__(self, gen):
        self.gen = gen
        self.simp = BodySimplifier(gen.body)
        self.stop = False

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

class ForSimplifier:

    def next(self):
        if self.simp:
            try:
                self.simp.next()
                return
            except StopIteration:
                self.simp = None
        if self.stop:
            raise StopIteration
        self.gen.only_body = True
        self.stop = True

    def undo(self):
        if self.simp:
            self.simp.undo()
        else:
            self.gen.only_body = False

    def __init__(self, gen):
        self.gen = gen
        self.simp = BodySimplifier(gen.body)
        self.stop = False

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

class AssignSimplifier:

    def next(self):
        raise StopIteration()

    def undo(self):
        if self.simp:
            self.simp.undo()
        else:
            pass

    def __init__(self, gen):
        self.gen = gen
        self.cur = 0
        self.keep = None
        self.simp = None

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

class BodySimplifier:

    def next(self):
        if self.simp:
            try:
                self.simp.next()
                return
            except StopIteration:
                self.simp = None
        if self.valid >= len(self.gen.stmnts):
            raise StopIteration()
        self.keep = self.gen.stmnts.pop(self.valid)
        self.prev = self.valid
        return self.gen

    def undo(self):
        if self.simp:
            self.simp.undo()
        else:
            self.gen.stmnts.insert(self.valid, self.keep)
            self.keep = None
            self.valid += 1
            if isinstance(self.gen.stmnts[self.prev], For):
                self.simp = ForSimplifier(self.gen.stmnts[self.prev])
            elif isinstance(self.gen.stmnts[self.prev], If):
                self.simp = IfSimplifier(self.gen.stmnts[self.prev])
            else:
                self.simp = AssignSimplifier(self.gen.stmnts[self.prev])

    def __init__(self, gen):
        self.gen = gen
        self.valid = 0
        self.keep = None
        self.simp = None

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

class NumberSimplifier:

    def next(self):
        if not self.tries:
            raise StopIteration
        self.prev = self.gen.value
        self.gen.value = self.tries.pop()

    def undo(self):
        self.gen.value = self.prev

    def __init__(self, gen):
        self.gen = gen
        self.tries = [0, 1, 2]

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

class MainSimplifier:

    def next(self):
        try:
            self.simp.next()
        except StopIteration:
            if not self.simps:
                raise StopIteration
            self.simp = self.simps.pop()
            self.simp.next()
        return self.gen

    def undo(self):
        self.simp.undo()

    def __init__(self, gen):
        self.gen = gen
        self.simps = [NumberSimplifier(x) for x in gen.values]
        self.simps.append(BodySimplifier(gen.body))
        self.simp = self.simps.pop()

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

class RunCmd(threading.Thread):
    def __init__(self, cmd, timeout, file=None):
        threading.Thread.__init__(self)
        self.cmd = cmd
        self.timeout = timeout
        self.file = file

    def run(self):
        self.p = subprocess.Popen(self.cmd, stdout=self.file)
        self.p.wait()

    def Run(self):
        self.start()
        self.join(self.timeout)

        if self.is_alive():
            self.p.terminate()
            self.join()


def try_run(gen):
    with open('src/test.c', 'w') as file:
        print(str(gen), file=file)
    with open('compile.log', 'w') as file:
        ret1 = subprocess.call(['make', 'test.s'], stdout=file, stderr=file)
        if ret1:
            print("bug in this program")
            print(gen)
            exit()
        #subprocess.call(['python3', 'main.py', 'test.s', '-o', 'test2.s'],
        #        stdout=file, stderr=file)
        RunCmd(['python3', 'main.py', 'test.s', '-o', 'test2.s'], 60).Run()
        ret1 = subprocess.call(['make', 'test'], stdout=file, stderr=file)
        ret2 = subprocess.call(['make', 'test2', 'GENERIC=true'], stdout=file, stderr=file)
        if ret1 != ret2 :
            return gen
        file1 = open('test1.txt', 'w')
        with open('test1.txt','w') as file:
            p1 = RunCmd(['make', 'run-test'], 60, file).Run()
        with open('test2.txt','w') as file:
            p2 = RunCmd(['make', 'run-test2'], 60, file).Run()
        with open('test1.txt', 'r') as file:
            regs1 = file.readlines()[-10]
        with open('test2.txt', 'r') as file:
            regs2 = file.readlines()[-10]
        match1 = re.search(r"r0.3\s+=\s+([0-9a-f]+)", regs1)
        match2 = re.search(r"r0.3\s+=\s+([0-9a-f]+)", regs2)
        if not (match1 and match2):
            return gen
        print(match1.group(1), match2.group(1))
        if match1.group(1) != match2.group(1):
            return gen
        return

def main():
    for i in range(100):
        gen = Main()
        result = try_run(gen)
        if result:
            print("\nFault detected after {0} tests.".format(i))
            with open('src/orig.c', 'w') as file:
                print(gen, file=file)
            simp = MainSimplifier(gen)
            for gen in simp:
                if not try_run(gen):
                    simp.undo()
                    print('.', end='')
                else:
                    print('x', end='')
                sys.stdout.flush()
            with open('src/simp.c', 'w') as file:
                print(gen, file=file)
            return
        else:
            print('.', end='')
            sys.stdout.flush()
    print('\nSuccessfully completed {0} tests.'.format(i))



if __name__ == '__main__':
    main()
