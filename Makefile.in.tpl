
.PHONY: all
all:
	# vexparse is interpreted! No compilation needed!

.PHONY: install
install: install-{target}

.PHONY: install-vexparse
install-vexparse:
	mkdir -p @prefix@/lib/vexparse
	cp -f -t @prefix@/lib/vexparse @srcdir@/*.py
	mkdir -p @prefix@/bin
	cp -f -t @prefix@/bin @srcdir@/rvex-elf32-as
	ln -f -s ../lib/vexparse/main.py @prefix@/bin/vexparse

.PHONY: install-no-vexparse
install-no-vexparse:
	mkdir -p @prefix@/bin
	ln -f -s rvex-elf32-ras @prefix@/bin/rvex-elf32-as
