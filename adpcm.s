	.section .text
	.proc 
adpcm_coder::
	c0		ldb	$r0.14 = 0x2[$r0.6]
	c0		mov	$r0.12 = stepsizeTable
	c0		cmple	$b0.1 = $r0.5,0
;;
	c0		mov	$r0.19 = 1
	c0		mov	$r0.22 = indexTable
;;
	c0		ldh	$r0.18 = 0x0[$r0.6]
;;
	c0		sh2add	$r0.13 = $r0.14,$r0.12
	c0		br	$b0.1,L2
;;
;;
;;
	c0		ldw	$r0.15 = 0x0[$r0.13]
	c0		goto	L13
;;
;;
;;
L22:
	c0		sub	$r0.13 = $r0.13,$r0.15
	c0		add	$r0.16 = $r0.16,$r0.15
	c0		mov	$r0.21 = 6
	c0		mov	$r0.17 = 4
;;
;;
;;
L4:
	c0		shr	$r0.15 = $r0.15,1
;;
;;
;;
	c0		cmplt	$b0.1 = $r0.13,$r0.15
;;
;;
;;
	c0		br	$b0.1,L5
;;
;;
;;
	c0		sub	$r0.13 = $r0.13,$r0.15
	c0		add	$r0.16 = $r0.16,$r0.15
	c0		mov	$r0.17 = $r0.21
;;
;;
;;
L5:
	c0		shr	$r0.15 = $r0.15,1
;;
;;
;;
	c0		cmplt	$b0.1 = $r0.13,$r0.15
;;
;;
;;
	c0		br	$b0.1,L6
;;
;;
;;
	c0		or	$r0.17 = $r0.17,1
	c0		add	$r0.16 = $r0.16,$r0.15
;;
;;
;;
L6:
	c0		cmpeq	$b0.1 = $r0.20,0
;;
;;
;;
	c0		br	$b0.1,L7
;;
;;
;;
	c0		or	$r0.17 = $r0.17,$r0.20
	c0		sub	$r0.18 = $r0.18,$r0.16
;;
;;
;;
	c0		sh2add	$r0.13 = $r0.17,$r0.22
	c0		max	$r0.18 = $r0.18,-32768
;;
;;
;;
	c0		ldw	$r0.13 = 0x0[$r0.13]
	c0		min	$r0.18 = $r0.18, 32767
;;
;;
;;
	c0		add	$r0.14 = $r0.14,$r0.13
;;
;;
;;
	c0		cmplt	$b0.1 = $r0.14,0
;;
;;
;;
	c0		br	$b0.1,L16
;;
;;
;;
L23:
	c0		min	$r0.14 = $r0.14, 88
;;
;;
;;
L10:
	c0		sh2add	$r0.13 = $r0.14,$r0.12
	c0		cmpeq	$b0.1 = $r0.19,0
;;
;;
;;
	c0		ldw	$r0.15 = 0x0[$r0.13]
	c0		br	$b0.1,L11
;;
;;
;;
	c0		add	$r0.5 = $r0.5,-1
	c0		shl	$r0.17 = $r0.17,4
	c0		xor	$r0.19 = $r0.19,1
;;
;;
;;
	c0		cmpne	$b0.1 = $r0.5,0
	c0		and	$r0.11 = $r0.17,255
;;
;;
;;
	c0		brf	$b0.1, L20
;;
;;
;;
L13:
	c0		ldh	$r0.13 = 0x0[$r0.3]
	c0		add	$r0.3 = $r0.3,2
	c0		mov	$r0.20 = 0
;;
;;
;;
	c0		sub	$r0.13 = $r0.13,$r0.18
;;
;;
;;
	c0		cmpge	$b0.1 = $r0.13,0
;;
;;
;;
	c0		brf	$b0.1, L21
;;
;;
;;
L3:
	c0		cmplt	$b0.1 = $r0.13,$r0.15
	c0		shr	$r0.16 = $r0.15,3
;;
;;
;;
	c0		brf	$b0.1, L22
;;
;;
;;
	c0		mov	$r0.21 = 2
	c0		mov	$r0.17 = 0
	c0		goto	L4
;;
;;
;;
L11:
	c0		and	$r0.17 = $r0.17,15
	c0		add	$r0.5 = $r0.5,-1
	c0		xor	$r0.19 = $r0.19,1
;;
;;
;;
	c0		or	$r0.17 = $r0.17,$r0.11
	c0		cmpne	$b0.1 = $r0.5,0
;;
;;
;;
	c0		stb	0x0[$r0.4] = $r0.17
	c0		add	$r0.4 = $r0.4,1
;;
	c0		br	$b0.1,L13
;;
;;
;;
L20:
	c0		cmpne	$b0.1 = $r0.19,0
;;
;;
;;
	c0		br	$b0.1,L2
;;
;;
;;
	c0		stb	0x0[$r0.4] = $r0.11
;;
;;
;;
L2:
	c0		sth	0x0[$r0.6] = $r0.18
;;
	c0		stb	0x2[$r0.6] = $r0.14
;;
;;
;;
	c0		return	$r0.1 = $r0.1,(0x0),$l0.0
;;
;;
;;
;;
;;
L7:
	c0		or	$r0.17 = $r0.17,$r0.20
	c0		add	$r0.18 = $r0.16,$r0.18
;;
;;
;;
	c0		sh2add	$r0.13 = $r0.17,$r0.22
	c0		max	$r0.18 = $r0.18,-32768
;;
;;
;;
	c0		ldw	$r0.13 = 0x0[$r0.13]
	c0		min	$r0.18 = $r0.18, 32767
;;
;;
;;
	c0		add	$r0.14 = $r0.14,$r0.13
;;
;;
;;
	c0		cmplt	$b0.1 = $r0.14,0
;;
;;
;;
	c0		brf	$b0.1, L23
;;
;;
;;
L16:
	c0		mov	$r0.14 = 0
	c0		goto	L10
;;
;;
;;
L21:
	c0		sub	$r0.13 = 0,$r0.13
	c0		mov	$r0.20 = 8
	c0		goto	L3
;;
;;
;;
	.endp 
	.proc 
adpcm_decoder::
	c0		ldb	$r0.14 = 0x2[$r0.6]
	c0		mov	$r0.12 = stepsizeTable
	c0		cmple	$b0.1 = $r0.5,0
;;
	c0		mov	$r0.18 = 0
	c0		mov	$r0.21 = indexTable
;;
	c0		ldh	$r0.17 = 0x0[$r0.6]
;;
	c0		sh2add	$r0.13 = $r0.14,$r0.12
	c0		br	$b0.1,L25
;;
;;
;;
	c0		ldw	$r0.16 = 0x0[$r0.13]
	c0		goto	L26
;;
;;
;;
L40:
	c0		and	$r0.15 = $r0.11,15
;;
;;
;;
L29:
	c0		shr	$r0.13 = $r0.16,3
	c0		and	$r0.19 = $r0.15,4
	c0		and	$r0.20 = $r0.15,2
;;
;;
;;
	c0		cmpeq	$b0.2 = $r0.19,0
	c0		cmpeq	$b0.1 = $r0.20,0
	c0		add	$r0.19 = $r0.13,$r0.16
;;
;;
;;
	c0		slctf	$r0.13 = $b0.2,$r0.19,$r0.13
	c0		br	$b0.1,L31
;;
;;
;;
	c0		shr	$r0.19 = $r0.16,1
;;
;;
;;
	c0		add	$r0.13 = $r0.13,$r0.19
;;
;;
;;
L31:
	c0		and	$r0.19 = $r0.15,1
;;
;;
;;
	c0		cmpeq	$b0.1 = $r0.19,0
;;
;;
;;
	c0		br	$b0.1,L32
;;
;;
;;
	c0		shr	$r0.16 = $r0.16,2
;;
;;
;;
	c0		add	$r0.13 = $r0.13,$r0.16
;;
;;
;;
L32:
	c0		and	$r0.16 = $r0.15,8
;;
;;
;;
	c0		cmpeq	$b0.1 = $r0.16,0
;;
;;
;;
	c0		br	$b0.1,L33
;;
;;
;;
	c0		sh2add	$r0.15 = $r0.15,$r0.21
	c0		sub	$r0.17 = $r0.17,$r0.13
;;
;;
;;
	c0		ldw	$r0.15 = 0x0[$r0.15]
	c0		max	$r0.17 = $r0.17,-32768
;;
;;
;;
	c0		add	$r0.14 = $r0.14,$r0.15
	c0		min	$r0.17 = $r0.17, 32767
;;
;;
;;
	c0		cmplt	$b0.1 = $r0.14,0
;;
;;
;;
	c0		br	$b0.1,L37
;;
;;
;;
L41:
	c0		min	$r0.14 = $r0.14, 88
	c0		add	$r0.5 = $r0.5,-1
	c0		sxth	$r0.13 = $r0.17
;;
;;
;;
	c0		sh2add	$r0.15 = $r0.14,$r0.12
	c0		cmpeq	$b0.1 = $r0.5,0
	c0		sth	0x0[$r0.4] = $r0.13
;;
;;
;;
	c0		ldw	$r0.16 = 0x0[$r0.15]
	c0		br	$b0.1,L27
;;
;;
;;
L42:
	c0		xor	$r0.18 = $r0.18,1
	c0		add	$r0.4 = $r0.4,2
;;
;;
;;
L26:
	c0		cmpeq	$b0.1 = $r0.18,0
;;
;;
;;
	c0		brf	$b0.1, L40
;;
;;
;;
	c0		ldb	$r0.11 = 0x0[$r0.3]
	c0		add	$r0.3 = $r0.3,1
;;
;;
;;
	c0		shr	$r0.15 = $r0.11,4
;;
;;
;;
	c0		and	$r0.15 = $r0.15,15
	c0		goto	L29
;;
;;
;;
L33:
	c0		sh2add	$r0.15 = $r0.15,$r0.21
	c0		add	$r0.17 = $r0.13,$r0.17
;;
;;
;;
	c0		ldw	$r0.15 = 0x0[$r0.15]
	c0		max	$r0.17 = $r0.17,-32768
;;
;;
;;
	c0		add	$r0.14 = $r0.14,$r0.15
	c0		min	$r0.17 = $r0.17, 32767
;;
;;
;;
	c0		cmplt	$b0.1 = $r0.14,0
;;
;;
;;
	c0		brf	$b0.1, L41
;;
;;
;;
L37:
	c0		mov	$r0.14 = 0
	c0		add	$r0.5 = $r0.5,-1
	c0		sxth	$r0.13 = $r0.17
;;
;;
;;
	c0		sh2add	$r0.15 = $r0.14,$r0.12
	c0		cmpeq	$b0.1 = $r0.5,0
	c0		sth	0x0[$r0.4] = $r0.13
;;
;;
;;
	c0		ldw	$r0.16 = 0x0[$r0.15]
	c0		brf	$b0.1, L42
;;
;;
;;
L27:
	c0		sth	0x0[$r0.6] = $r0.13
;;
	c0		stb	0x2[$r0.6] = $r0.14
;;
;;
;;
	c0		return	$r0.1 = $r0.1,(0x0),$l0.0
;;
;;
;;
;;
;;
L25:
	c0		sxth	$r0.13 = $r0.17
	c0		stb	0x2[$r0.6] = $r0.14
;;
;;
;;
	c0		sth	0x0[$r0.6] = $r0.13
;;
;;
;;
	c0		return	$r0.1 = $r0.1,(0x0),$l0.0
;;
;;
;;
;;
;;
	.endp 
	.proc 
main::
	c0		stw	-0x4[$r0.1] = $l0.0
	c0		mov	$r0.3 = pcmdata
	c0		add	$r0.1 = $r0.1,-32
;;
	c0		mov	$r0.4 = adpcmdata
	c0		mov	$r0.5 = 10
;;
	c0		mov	$r0.6 = coder_state
	c0		call	$l0.0 = adpcm_coder
;;
	c0		mov	$r0.3 = adpcmdata
	c0		mov	$r0.4 = pcmdata_2
;;
	c0		mov	$r0.5 = 10
	c0		mov	$r0.6 = decoder_state
	c0		call	$l0.0 = adpcm_decoder
;;
	c0		ldw	$r0.11 = coder_state[$r0.0]
	c0		mov	$r0.3 = -3
;;
;;
;;
	c0		and	$r0.11 = $r0.11,16777215
;;
;;
;;
	c0		cmpne	$b0.1 = $r0.11,65568
;;
;;
;;
	c0		brf	$b0.1, L49
;;
;;
;;
L44:
	c0		ldw	$l0.0 = 0x1c[$r0.1]
;;
;;
;;
	c0		return	$r0.1 = $r0.1,(0x20),$l0.0
;;
;;
;;
;;
;;
L49:
	c0		ldw	$r0.11 = decoder_state[$r0.0]
	c0		mov	$r0.3 = -2
;;
;;
	c0		ldw	$l0.0 = 0x1c[$r0.1]
;;
	c0		and	$r0.11 = $r0.11,16777215
;;
;;
;;
	c0		cmpne	$b0.1 = $r0.11,65568
;;
;;
;;
	c0		brf	$b0.1, L50
;;
;;
	c0		return	$r0.1 = $r0.1,(0x20),$l0.0
;;
;;
;;
;;
;;
L50:
	c0		mov	$r0.11 = 0
;;
;;
;;
	c0		mov	$r0.3 = $r0.11
;;
;;
;;
L45:
	c0		ldh	$r0.13 = pcmdata_2[$r0.11]
;;
;;
	c0		ldh	$r0.12 = pcmdata_2_ref[$r0.11]
	c0		add	$r0.11 = $r0.11,2
;;
;;
;;
	c0		cmpne	$b0.1 = $r0.13,$r0.12
;;
;;
;;
	c0		br	$b0.1,L44
;;
;;
;;
	c0		add	$r0.3 = $r0.3,1
;;
;;
;;
	c0		cmpne	$b0.1 = $r0.3,10
;;
;;
;;
	c0		br	$b0.1,L45
;;
;;
;;
	c0		mov	$r0.3 = -1
	c0		ldw	$l0.0 = 0x1c[$r0.1]
;;
;;
;;
	c0		return	$r0.1 = $r0.1,(0x20),$l0.0
;;
;;
;;
;;
;;
	.endp 
	.section .data
	.comm	decoder_state,	4,	32
	.section .data
	.comm	coder_state,	4,	32
	.section .data
pcmdata_2_ref::
	.data2	0
	.data2	0
	.data2	11
	.data2	17
	.data2	16
	.data2	23
	.data2	24
	.data2	25
	.data2	33
	.data2	32
adpcmdata_ref::
	.data1	0
	.data1	113
	.data1	-126
	.data1	0
	.data1	56
	.section .data
	.comm	pcmdata_2,	20,	32
	.section .data
	.comm	adpcmdata,	5,	32
pcmdata::
	.data2	0
	.data2	0
	.data2	16
	.data2	16
	.data2	16
	.data2	24
	.data2	24
	.data2	24
	.data2	32
	.data2	32
	.section	.data
stepsizeTable::
	.data4	7
	.data4	8
	.data4	9
	.data4	10
	.data4	11
	.data4	12
	.data4	13
	.data4	14
	.data4	16
	.data4	17
	.data4	19
	.data4	21
	.data4	23
	.data4	25
	.data4	28
	.data4	31
	.data4	34
	.data4	37
	.data4	41
	.data4	45
	.data4	50
	.data4	55
	.data4	60
	.data4	66
	.data4	73
	.data4	80
	.data4	88
	.data4	97
	.data4	107
	.data4	118
	.data4	130
	.data4	143
	.data4	157
	.data4	173
	.data4	190
	.data4	209
	.data4	230
	.data4	253
	.data4	279
	.data4	307
	.data4	337
	.data4	371
	.data4	408
	.data4	449
	.data4	494
	.data4	544
	.data4	598
	.data4	658
	.data4	724
	.data4	796
	.data4	876
	.data4	963
	.data4	1060
	.data4	1166
	.data4	1282
	.data4	1411
	.data4	1552
	.data4	1707
	.data4	1878
	.data4	2066
	.data4	2272
	.data4	2499
	.data4	2749
	.data4	3024
	.data4	3327
	.data4	3660
	.data4	4026
	.data4	4428
	.data4	4871
	.data4	5358
	.data4	5894
	.data4	6484
	.data4	7132
	.data4	7845
	.data4	8630
	.data4	9493
	.data4	10442
	.data4	11487
	.data4	12635
	.data4	13899
	.data4	15289
	.data4	16818
	.data4	18500
	.data4	20350
	.data4	22385
	.data4	24623
	.data4	27086
	.data4	29794
	.data4	32767
indexTable::
	.data4	-1
	.data4	-1
	.data4	-1
	.data4	-1
	.data4	2
	.data4	4
	.data4	6
	.data4	8
	.data4	-1
	.data4	-1
	.data4	-1
	.data4	-1
	.data4	2
	.data4	4
	.data4	6
	.data4	8

