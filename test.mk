
ifndef GENERIC
GENERIC = false
endif

AS = rvex-elf32-as
LD = rvex-elf32-ld
CC = /data/vex-3.43/bin/cc
SIM = xstsim
CFLAGS = -fno-xnop -autoinline -fexpand-div -O3 -fmm=pipe_1_8_fw.mm
ifeq (GENERIC, true)
ASFLAGS = --issue 8 --borrow 1.0.3.2.5.4.7.6. --config ffffffff -u --no-pad --some-pad
SIMFLAGS = --ips='"[r-VEX c]"'  --c.trace=1 --c.trace_regs=2
else
ASFLAGS = --issue 8 --borrow 1.0.3.2.5.4.7.6. --config ffffffff
SIMFLAGS = --ips='"[r-VEX c]"'  --c.trace=1 --c.trace_regs=2 --c.core.issue_width=8
endif
SRCDIR = ./src
PWD = $(shell pwd)

EXECUTABLES = test simp

.SUFFIXES:
.PRECIOUS: %.o %.s

.PHONY: all
all: $(EXECUTABLES)

_start.s: $(SRCDIR)/_start.s
	cp $^ $@

%.s: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) -S $<

%.o: %.s
	$(AS) $(ASFLAGS) $< -o $@

%: %.o _start.o
	$(LD) $^ -o $@

.PHONY: run
run: $(EXECUTABLES:%=run-%)

run-% : %
	$(SIM) $(SIMFLAGS) --c.target_exec='"$^"'

.PHONY: clean
clean:
	$(RM) $(EXECUTABLES) *.o *.s *.vhd *.h *.hex *.map
