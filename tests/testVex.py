import unittest
import vex


class TestArgument(unittest.TestCase):

    def setUp(self):
        pass

    def testMakeArgument(self):
        pass


class TestRegister(unittest.TestCase):

    def setUp(self):
        self.r1 = vex.parse_register("$r0.1")
        self.r2 = vex.parse_register("$r0.2")
        self.r3 = vex.parse_register("$r0.1")
        self.b1 = vex.parse_register('$b0.1')

    def test_register(self):
        self.assertEqual(self.r1.c, 0)
        self.assertEqual(self.r1.t, 'r')
        self.assertEqual(self.r1.n, 1)

    def test_equality(self):
        self.assertEqual(self.r1, self.r1)
        self.assertNotEqual(self.r1, self.r2)
        self.assertEqual(self.r1, self.r3)
        self.assertLess(self.r1, self.r2)
        self.assertGreater(self.r2, self.r1)
        self.assertNotEqual(self.r1, self.b1)

    def test_get_free_reg(self):
        free = self.b1.get_free_reg([])
        self.assertEqual(free, vex.BranchRegister(0, 0))

if (__name__ == '__main__'):
    try:
        unittest.main()
    except SystemExit as inst:
        pass
