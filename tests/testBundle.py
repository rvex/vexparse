import unittest
from bundle import Label


class TestLabel(unittest.TestCase):

    def setUp(self):
        self.local_label = Label('L1:')
        self.global_label = Label('L?5::')

    def test_label(self):
        self.assertEqual(self.local_label.label, 'L1')
        self.assertEqual(self.global_label.label, 'L?5')

    def test_scope(self):
        self.assertEqual(self.local_label.local, True)
        self.assertEqual(self.global_label.local, False)

    def test_str(self):
        self.assertEqual(str(self.local_label), "L1:")
        self.assertEqual(str(self.global_label), "L?5::")

if (__name__ == '__main__'):
    try:
        unittest.main()
    except SystemExit as inst:
        pass
