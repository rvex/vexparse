import unittest
import parse

class TestParse(unittest.TestCase):

    def setUp(self):
        pass

    def test_is_label(self):
        self.assertEqual(parse.is_label("L1:"), True)

    def test_is_end_bundle(self):
        self.assertEqual(parse.is_end_bundle(";;"), True)

    def test_is_start_function(self):
        self.assertEqual(parse.is_start_function(".proc"), True)
    
    def test_is_end_function(self):
        self.assertEqual(parse.is_end_function(".endp"), True)

    def test_is_register(self):
        self.assertEqual(parse.is_register("$r0.0"), True)

    def test_is_store(self):
        self.assertEqual(parse.is_store("stw"), True)

    def test_get_cluster(self):
        self.assertEqual(parse.get_cluster("c0 mov $r0.1 = $r0.2"),
                         (0, " mov $r0.1 = $r0.2"))
        self.assertEqual(parse.get_cluster("c0		cmple	$b0.1 = $r0.5,0"),
                         (0, "		cmple	$b0.1 = $r0.5,0"))

    def test_get_mnemonic(self):
        self.assertEqual(parse.get_mnemonic("mov $r0.1 = $r0.2"),
                         ("mov", " $r0.1 = $r0.2"))

if (__name__ == '__main__'):
    unittest.main()
