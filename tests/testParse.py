import unittest
import main
import vex


class TestInstruction(unittest.TestCase):

    def setUp(self):
        self.mov = main.parse_instruction("c0 mov $r0.12 = 0", "")
        self.br = main.parse_instruction("c0 br $b0.1,L5")
        self.brf = main.parse_instruction("c0 brf $b0.1,L5")
        self.ret = main.parse_instruction("c0 return $r0.1 = $r0.1,(0x0),$l0.0")
        self.goto = main.parse_instruction("c0 goto L5")
        self.addcg = main.parse_instruction(
            "c0 addcg $r0.1, $b0.1 = $b0.1, $r0.1, $r0.2")
        self.stb = main.parse_instruction("c0 stb 0x2[$r0.6] = $r0.14")
        self.ldw = main.parse_instruction("c0 ldw $r0.13 = 0x0[$r0.13]")

    def test_get_branch_destination(self):
        self.assertEqual(self.br.get_branch_destination(), ["next", "L5"])
        self.assertEqual(self.goto.get_branch_destination(), ["L5"])

    def test_is_branch(self):
        self.assertTrue(self.br.is_branch())
        self.assertTrue(self.brf.is_branch())
        self.assertFalse(self.mov.is_branch())

    def test_get_written(self):
        self.assertEqual(
            set(str(x) for x in self.addcg.get_written_registers()),
            set(['$r0.1', '$b0.1']))
        self.assertEqual(set(self.stb.get_written_registers()), set([]))

    def test_get_read(self):
        self.assertEqual(
            set(str(x) for x in self.addcg.get_read_registers()),
            set(['$r0.1', '$b0.1', '$r0.2']))
        self.assertEqual(
            set(str(x) for x in self.br.get_read_registers()),
            set(['$b0.1']))
        self.assertEqual(
            set(str(x) for x in self.stb.get_read_registers()),
            set(["$r0.6", "$r0.14"]))

    def test_instruction(self):
        self.assertEqual(self.addcg.cluster, 0)
        self.assertEqual(self.addcg.mnemonic, 'addcg')
        self.assertEqual(set(str(x) for x in self.addcg.dests),
                         set(['$r0.1', '$b0.1']))
        self.assertEqual(set(str(x) for x in self.addcg.srcs),
                         set(['$b0.1', '$r0.1', '$r0.2']))
        self.assertEqual(str(self.addcg),
                         "c0 addcg $r0.1, $b0.1 = $b0.1, $r0.1, $r0.2")

    def test_change_source_reg(self):
        insn = self.addcg
        insn.change_source_reg(vex.parse_register('$r0.1'),
                               vex.parse_register('$r0.5'))
        new_insn = 'c0 addcg $r0.1, $b0.1 = $b0.1, $r0.5, $r0.2'
        self.assertEqual(str(insn), new_insn)
        self.stb.change_source_reg(vex.parse_register("$r0.6"),
                                   vex.parse_register("$r0.7"))
        self.assertEqual(str(self.stb), "c0 stb 0x2[$r0.7] = $r0.14")

    def test_change_dest_reg(self):
        insn = self.addcg
        insn.change_dest_reg(vex.parse_register('$b0.1'), vex.parse_register('$b0.5'))
        new_insn = 'c0 addcg $r0.1, $b0.5 = $b0.1, $r0.1, $r0.2'
        self.assertEqual(str(insn), new_insn)

    def test_str(self):
        self.assertEqual(str(self.stb), "c0 stb 0x2[$r0.6] = $r0.14")
        self.assertEqual(str(self.ldw), "c0 ldw $r0.13 = 0x0[$r0.13]")

    def test_has_long_imm(self):
        self.assertFalse(self.stb.has_long_imm())
        mov = main.parse_instruction("c0 mov $r0.12 = 1000")
        self.assertTrue(mov.has_long_imm())
        self.assertFalse(self.goto.has_long_imm())
        ldw = main.parse_instruction("c0 ldw $r0.13 = bla [$r0.13]")
        self.assertTrue(ldw.has_long_imm())
        mov = main.parse_instruction("c0 mov $r0.11 = (~0x0)")
        self.assertFalse(mov.has_long_imm())



class TestInstructionBundle(unittest.TestCase):

    def setUp(self):
        self.brf = main.InstructionBundle(
            [main.parse_instruction("c0 brf $b0.1,L5")], 0, raw=True)
        self.br = main.InstructionBundle(
            [main.parse_instruction("c0 br $b0.0, L1")], 0, raw=True)
        self.mov = main.InstructionBundle(
            [main.parse_instruction("c0 mov $r0.12 = $r0.13")], 0, raw=True)
        self.ret = main.InstructionBundle(
            [main.parse_instruction("c0 return $r0.1 = $r0.1,0,$l0.0")], 0, raw=True)
        self.bundle = main.InstructionBundle(
            [main.parse_instruction(x) for x in [
            "c0 add $r0.11 = $r0.3, $r0.4",
            "c0 cmpeq $b0.0 = $r0.11, $r0.2",
            "c0 br $b0.0, L1"]], 0, raw=True)
        self.freeRegs = [vex.GeneralRegister(0, x) for x in range(16, 63)]

    def test_get_written(self):
        self.assertEqual(set(str(x) for x in self.mov.get_written()),
                         set(["$r0.12"]))
        self.assertEqual(set(self.br.get_written()), set([]))
        self.assertEqual(set(str(x) for x in self.ret.get_written()),
                         set(["$r0.1"]))

    def test_get_read(self):
        self.assertEqual(set(str(x) for x in self.mov.get_read()),
                         set(["$r0.13"]))
        self.assertEqual(set(str(x) for x in self.br.get_read()),
                         set(["$b0.0"]))
        self.assertEqual(set(str(x) for x in self.ret.get_read()),
                         set(["$r0.1", "$l0.0"]))

    def test_has_cycle(self):
        self.assertTrue(self.bundle.has_cycle())
        self.assertFalse(self.ret.has_cycle())

    def test_get_cycle_regs(self):
        regs = self.bundle.get_cycle_regs()
        self.assertEqual(set([str(x) for x in regs]), set(['$r0.11', '$b0.0']))

    def test_rename_written(self):
        new_bundle = main.InstructionBundle(
            [main.parse_instruction(x) for x in [
            "c0 add $r0.11 = $r0.3, $r0.4",
            "c0 cmpeq $b0.1 = $r0.11, $r0.2",
            "c0 br $b0.0, L1"]], 0, raw=True)
        self.assertNotEqual(self.bundle, new_bundle)
        self.bundle.rename_written(vex.parse_register('$b0.0'),
                                   vex.parse_register('$b0.1'))
        self.assertEqual(self.bundle, new_bundle)

    def test_has_load_dependency(self):
        bundle = main.InstructionBundle(
            [main.parse_instruction(x) for x in [
            "c0 ldw $r0.1 = 0[$r0.4]",
            "c0 cmpeq $b0.0 = $r0.1, $r0.2"]], 0, raw=True)
        self.assertTrue(bundle.has_load_dependency())


class TestFunction(unittest.TestCase):
    def setUp(self):
        lines = [
            '.proc',
            '.entry arg($r0.6, $r0.14, $r0.15, $r0.13, $b0.1)',
            'c0		ldb	$r0.14 = 0x2[$r0.6]',
            'c0		mov	$r0.12 = stepsizeTable',
            'c0     cmpeq   $b0.1 = $r0.14, 0',
            'c0		br	$b0.1,L5',
            ';;',
            'c0		mov	$r0.19 = $r0.12',
            'c0		mov	$r0.12 = indexTable',
            ';;',
            'L5:',
            'c0		sub	$r0.13 = $r0.13,$r0.15',
            ';;',
            'c0		mov	$r0.21 = 6',
            'c0		slct	$r0.3 = $b0.1, $r0.12, 4',
            ';;',
            '.return ret($r0.3)',
            'c0		return	$r0.1 = $r0.1,(0x0),$l0.0',
            ';;',
            '.endp.']
        self.func = main.read_file(lines)[1]
        self.renamed = main.read_file([
            '.proc',
            'c0		ldb	$r0.14 = 0x2[$r0.6]',
            'c0		mov	$r0.11 = stepsizeTable',
            'c0         cmpeq   $b0.0 = $r0.14, 0',
            'c0		br	$b0.1,L5',
            ';;',
            'c0		mov	$r0.19 = $r0.11',
            'c0		mov	$r0.11 = indexTable',
            ';;',
            'L5:',
            'c0		sub	$r0.13 = $r0.13,$r0.15',
            ';;',
            'c0		mov	$r0.21 = 6',
            'c0		slct	$r0.3 = $b0.0, $r0.11, 4',
            ';;',
            '.return ret($r0.3)',
            'c0		return	$r0.1 = $r0.1,(0x0),$l0.0',
            ';;',
            '.endp'])[1]
        self.renamed2 = main.read_file([
            '.proc',
            'c0		ldb	$r0.14 = 0x2[$r0.6]',
            'c0		mov	$r0.12 = stepsizeTable',
            'c0         cmpeq   $b0.0 = $r0.14, 0',
            'c0		br	$b0.1,L5',
            ';;',
            'c0		mov	$r0.19 = $r0.12',
            'c0		mov	$r0.12 = indexTable',
            ';;',
            'L5:',
            'c0		sub	$r0.13 = $r0.13,$r0.15',
            ';;',
            'c0		mov	$r0.21 = 6',
            'c0		slct	$r0.3 = $b0.0, $r0.12, 4',
            ';;',
            '.return ret($r0.3)',
            'c0		return	$r0.1 = $r0.1,(0x0),$l0.0',
            ';;',
            '.endp'])[1]

    def test_get_free_list(self):
        pass

    def test_get_read(self):
        self.assertEqual(self.func.get_read(vex.parse_register('$b0.1'), 1), {4})
        self.assertEqual(self.func.get_read(vex.parse_register('$r0.9'), 1),
                set())
        self.assertEqual(self.func.get_read(vex.parse_register('$r0.12'), 1),
                {2, 4})

    def test_get_written(self):
        self.assertEqual(
            self.func.get_written(vex.parse_register('$r0.12'), 4), {1, 2})
        self.assertEqual(
            self.func.get_written(vex.parse_register('$b0.1'), 4), {1})

    def test_register_live_table(self):
        table = self.func.build_register_live_table()
        table = {key:value for key,value in table.items()}
        self.maxDiff = None
        self.assertEqual({
            0: set([vex.parse_register('$r0.1'),
                    vex.parse_register('$l0.0')]),
            1: set([vex.parse_register('$r0.15'), vex.parse_register('$r0.14'),
                    vex.parse_register('$r0.6'), vex.parse_register('$r0.1'),
                    vex.parse_register('$r0.13'), vex.parse_register('$b0.1'),
                    vex.parse_register('$l0.0')]),
            2: set([vex.parse_register('$r0.15'), vex.parse_register('$r0.1'),
                    vex.parse_register('$r0.13'), vex.parse_register('$l0.0'),
                    vex.parse_register('$r0.12'), vex.parse_register('$b0.1')]),
            3: set([vex.parse_register('$r0.15'), vex.parse_register('$r0.1'),
                    vex.parse_register('$r0.13'), vex.parse_register('$l0.0'),
                    vex.parse_register('$r0.12'), vex.parse_register('$b0.1')]),
            4: set([vex.parse_register('$r0.1'), vex.parse_register('$l0.0'),
                    vex.parse_register('$r0.12'), vex.parse_register('$b0.1')]),
            5: set([vex.parse_register('$r0.1'), vex.parse_register('$l0.0'),
                    vex.parse_register('$r0.3')]),
            6: set([vex.parse_register('$r0.3')])},
            table)

    def test_rewrite(self):
        self.func.rewrite(vex.parse_register('$r0.12'), 1)
        self.func.rewrite(vex.parse_register('$b0.1'), 1)
        self.assertEqual(self.func, self.renamed)

    def test_get_live_regs(self):
        self.assertEqual(self.func.get_free_reg(vex.parse_register('$r0.12'), {1}),
                         vex.parse_register('$r0.11'))
        self.assertEqual(self.func.get_free_reg(vex.parse_register('$b0.1'), {1}),
                         vex.parse_register('$b0.0'))
        pass

    def test_fix_cycles(self):
        self.assertNotEqual(self.renamed2, self.func)
        self.func.fix_cycles()
        self.assertEqual(self.renamed2, self.func)
        with open('tests/test3.s') as file1:
            with open('tests/test3_result.s') as file2:
                f1 = main.read_file(file1)[1]
                f2 = main.read_file(file2)[1]
                f1.fix_cycles()
                self.assertEqual(f1, f2)

    def test_fix_load_dependency(self):
        with open('tests/test2.s') as file1:
            with open('tests/test2_result.s') as file2:
                f1 = main.read_file(file1)[1]
                f2 = main.read_file(file2)[1]
                f1.fix_load_dependency()
                self.assertEqual(f1, f2)


class TestVexParse(unittest.TestCase):

    def test_function_count(self):
        with open('tests/test.s') as file:
            f = main.read_file(file)
            self.assertEqual(len(f), 3)

if (__name__ == '__main__'):
    try:
        unittest.main()
    except SystemExit as inst:
        pass
