import unittest
import main

class TestMultiWrite(unittest.TestCase):

    def setUp(self):
        input = '''
.proc
main::
ldw $r0.1 = 0[$r0.3]
;;
bla:
add $r0.1 = $r0.1, 0
;;
.endp

.proc
main::
ldw $r0.1 = 0[$r0.3]
goto bla
;;
bla:
add $r0.1 = $r0.1, 0
;;
.endp

.proc
main::
ldw $r0.1 = 0[$r0.3]
brf $b0.0, bla
;;
bla:
add $r0.1 = $r0.1, 0
;;
.endp

.proc
main::
ldw $r0.1 = a[$r0.3]
brf $b0.0, bla
;;
bla:
add $r0.1 = $r0.1, 0
;;
.endp

.proc
main:
ldw $r0.1 = 0[$r0.3]
;;
bla:
add $r0.2 = $r0.2, 0
;;
.endp
'''
        self.fs = []
        for f in main.read_file(input.splitlines()):
            if isinstance(f, main.Function):
                self.fs.append(f)

    def testCrossBlockFallThrough(self):
        self.assertEqual(len(self.fs[0].bundles), 4)
        self.fs[0].new_resched(main.default_config)
        self.assertEqual(len(self.fs[0].bundles), 5)

    def testCrossBlockGoto(self):
        self.assertEqual(len(self.fs[1].bundles), 4)
        self.fs[1].new_resched(main.default_config)
        self.assertEqual(len(self.fs[1].bundles), 4)

    def testCrossBlockBranch(self):
        self.assertEqual(len(self.fs[2].bundles), 4)
        self.fs[2].new_resched(main.default_config)
        self.assertEqual(len(self.fs[2].bundles), 5)

    def testCrossBlockBranchLongImm(self):
        self.assertEqual(len(self.fs[3].bundles[1].insns), 2)
        self.fs[3].new_resched(main.default_config)
        self.assertEqual(len(self.fs[3].bundles[1].insns), 1)

    def testCrossBlockNoDep(self):
        self.assertEqual(len(self.fs[4].bundles), 4)
        self.fs[4].new_resched(main.default_config)
        self.assertEqual(len(self.fs[4].bundles), 4)

if __name__ == '__main__':
    unittest.main()
