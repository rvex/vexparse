.proc
foo:
c0 return $r0.1 = $r0.1, 0, $l0.0
;;
.endp

.proc
main::
c0 add $r0.20 = $r0.0, 0
c0 add $r0.0 = $r0.20, 0
;;
L1:
c0 add $r0.21 = $r0.20, 10
;;
c0 mov $r0.11 = 100
;;
c0 add $r0.20 = $r0.21, 10
c0 br $b0.0, L1
;;
c0 mov $r0.3 = $r0.11
c0 return $r0.1 = $r0.1, 0, $l0.0
;;
.endp
