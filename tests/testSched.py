import unittest
import main

class TestSched(unittest.TestCase):

    def setUp(self):
        input = '''
.proc
main::
ldw $r0.1 = 0[$r0.3]
add $r0.4 = $r0.1, 0
call $l0.0 = bla
;;
.endp

.proc
main:
cmpeq   $b0.4=$r0.10, $r0.0
brf     $b0.3, .L__6_12
;;
bla:
brf     $b0.4, .L__6_12
;;
.endp

.proc
main:
add $r0.4 = $r0.4
;;
xor $r0.7 = $r0.7, $r0.4
brf $b0.1, L6?3
;;
shr $r0.4 = $r0.7, 8
;;
.endp
'''
        self.fs = []
        for f in main.read_file(input.splitlines()):
            if isinstance(f, main.Function):
                self.fs.append(f)

    def testSched1(self):
        #number of bundles should be 4:
        # Entry, Exit, Call, and actual bundle
        self.assertEqual(len(self.fs[0].bundles), 4)
        self.fs[0].new_resched(main.default_config)
        self.assertEqual(len(self.fs[0].bundles), 4)

    def testSched2(self):
        #number of bundles should be 4:
        # Entry, Exit, Call, and actual bundle
        self.assertEqual(len(self.fs[1].bundles), 4)
        self.fs[1].new_resched(main.default_config)
        self.assertEqual(len(self.fs[1].bundles), 4)

    def testSched3(self):
        #the position of the branch instruction should not change
        res1 = str(self.fs[2])
        self.fs[2].new_resched(main.default_config)
        res2 = str(self.fs[2])
        self.assertEqual(res1, res2)

if __name__ == '__main__':
    unittest.main()
