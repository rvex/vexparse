import unittest
import graph
import main

ALU = 1
MUL = 2
MEM = 4
BR = 8


class TestGraph(unittest.TestCase):

    def setUp(self):
        bundle = main.InstructionBundle([
            ("c0 add $r0.1 = $r0.3, $r0.4",""),
            ("c0 cmpeq $b0.0 = $r0.1, $r0.2",""),
            ("c0 br $b0.0, L1","")],1)
        bundle2 = main.InstructionBundle([
            ("c0 add $r0.1 = $r0.3, $r0.4",""),
            ("c0 cmpeq $r0.0 = $r0.1, $r0.2","")],1)
        bundle3 = main.InstructionBundle([],1)
        config = {
                'borrow': [[1],[0],[3],[2],[5],[4],[7],[6]],
                'layout': [{ALU, BR, MUL}, {ALU, MUL, MEM}, {ALU, MUL, BR}, {ALU, MUL},
                    {ALU, MUL, BR}, {ALU, MUL}, {ALU, MUL, BR}, {ALU, MUL}],
                'fus':    {ALU: 8, MUL: 4, MEM: 1, BR: 1},
                'opt':    0
                }
        self.graph1 = graph.Graph(bundle.insns, config)
        self.graph2 = graph.Graph(bundle2.insns, config)
        self.graph3 = graph.Graph(bundle3.insns, config)

    def test_schedule2(self):
        self.assertFalse(self.graph1.schedule2())
        self.assertTrue(self.graph2.schedule2())
        self.assertTrue(self.graph3.schedule2())

class TestInstruction(unittest.TestCase):

    def setUp(self):
        bundle = main.InstructionBundle([
            ("c0 add $r0.1 = $r0.3, $r0.4",""),
            ("c0 cmpeq $b0.0 = $r0.1, $r0.2",""),
            ("c0 br $b0.0, L1","")],1)
        bundle2 = main.InstructionBundle([
            ("c0 add $r0.1 = $r0.3, $r0.4",""),
            ("c0 cmpeq $r0.0 = $r0.1, $r0.2","")],1)
        bundle3 = main.InstructionBundle([],1)
        self.graph = graph.Graph(bundle.insns)
        self.graph2 = graph.Graph(bundle2.insns)
        self.graph3 = graph.Graph(bundle3.insns)

    def test_schedule(self):
        self.assertTrue(self.graph.schedule())
        self.assertEqual(self.graph2.schedule(), [])
        self.assertEqual(self.graph3.schedule(), [])

if (__name__ == '__main__'):
    unittest.main()
