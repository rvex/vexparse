import unittest
import main

class TestCombineReturn(unittest.TestCase):

    def setUp(self):
        input = '''
.proc
main::
add $r0.1 = $r0.1, 10
return $l0.0
;;
.endp

.proc
main::
add $r0.1 = $r0.1, 12
return $r0.1 = $r0.1, 12, $l0.0
;;
.endp
'''
        self.fs = []
        for f in main.read_file(input.splitlines()):
            if isinstance(f, main.Function):
                self.fs.append(f)

    def testCombine1(self):
        self.fs[0].fix_return_and_stack_pop()
        self.assertEqual(str(self.fs[0].bundles[1].insns[0]),
                'c0 return $r0.1 = $r0.1, 10, $l0.0')

    def testCombine2(self):
        self.fs[1].fix_return_and_stack_pop()
        self.assertEqual(str(self.fs[1].bundles[1].insns[0]),
                'c0 return $r0.1 = $r0.1, 12 + 12, $l0.0')

if __name__ == '__main__':
    unittest.main()
