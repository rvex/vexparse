import unittest
import main

class TestMultiWrite(unittest.TestCase):

    def setUp(self):
        input = '''
.proc
main::
c0    divs $r0.15, $b0.6 = $r0.15, $r0.7, $b0.0   ## bblock 1, line 116,  t216,  t222(I1),  t216,  t218,  t222(I1)
c0    cmplt $r0.22 = $r0.5, $r0.0   ## bblock 1, line 118,  t328,  10(SI32),  0(I32)
c0    cmplt $r0.46 = $r0.12, $r0.0   ## bblock 1, line 123,  t459,  100(SI32),  0(I32)
c0    sub $r0.45 = $r0.0, $r0.3   ## bblock 1, line 123,  t456,  0(I32),  t127
c0    mov $r0.48 = -10   ## bblock 1, line 123,  t471,  -10(SI32)
c0    mfb $r0.20 = $b0.2   ## t318(I1)
c0    mfb $r0.15 = $b0.6   ## t289(I1)
c0    stw 0x20[$r0.1] = $r0.11  ## spill ## t286
;;
add $r0.12 = $r0.15, 0
;;
.endp
'''
        self.f = main.read_file(input.splitlines())[1]

    def testMultiWrite(self):
        self.assertFalse(self.f.fix_same_reg_writes())
        self.assertEqual(str(self.f.bundles[1].insns[0].dests[0]), '$r0.0')

if __name__ == '__main__':
    unittest.main()
