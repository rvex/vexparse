import unittest
import main

class TestEmpty(unittest.TestCase):

    def setUp(self):
        input = '''.proc
        main::
        ;;
        .endp'''
        self.f = main.read_file(input.splitlines())[1]

    def testBla(self):
        self.f.new_resched(main.default_config)
        self.assertEqual(len(self.f.bundles), 3)


if __name__ == '__main__':
    unittest.main()



