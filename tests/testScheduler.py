import unittest
from scheduler import BundleScheduler
from main import default_config

class TestBundleScheduler(unittest.TestCase):

    def setUp(self):
        self.b = BundleScheduler(default_config)

    def test_cost(self):
        b = self.b
        self.assertEqual(b.cost(0), 3)
        self.assertEqual(b.cost(1), 3)
        self.assertEqual(b.cost(2), 3)
        self.assertEqual(b.cost(3), 4)
        self.assertEqual(b.cost(4), 4)
        self.assertEqual(b.cost(5), 6)
        self.assertEqual(b.cost(6), 6)
        self.assertEqual(b.cost(7), 7)
        self.assertEqual(b.cost(8), 7)

if (__name__ == '__main__'):
    try:
        unittest.main()
    except SystemExit as inst:
        pass
