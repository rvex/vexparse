.proc
main::
	c0    ldw $r0.2 = ((a + 0) + 0)[$r0.0]
;;
	c0    ldw $r0.4 = ((b + 0) + 0)[$r0.0]
;;
;;
;;
	c0    cmpgt $b0.0 = $r0.2, $r0.4
	c0    mpylu $r0.5 = $r0.2, $r0.4
	c0    mpyhs $r0.6 = $r0.2, $r0.4
;;
;;
	c0    add $r0.3 = $r0.5, $r0.6
	c0    brf $b0.0, L0?3
;;
	c0    return $r0.1 = $r0.1, (0x0), $l0.0
;;
L0?3:
	c0    add $r0.3 = $r0.4, $r0.2
	c0    return $r0.1 = $r0.1, (0x0), $l0.0
;;
.endp
